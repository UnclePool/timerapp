import Vue from 'vue'

export default {
  state: {
    timers: []
  },
  mutations: {
    setTimers (state, payload) {
      state.timers = payload.timers
    }
  },
  actions: {
    async getTimers ({ commit }) {
      const result = await Vue.axios.get('./static/timers.json')

      commit('setTimers', result.data)
    }
  }
}
